#include <stdio.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "md5.h"

const int PASS_LEN=50;     
const int HASH_LEN=33;    

struct entry 
{
    char password[PASS_LEN];
    char hash[HASH_LEN];
};

int crackerF(const void *a, const void *b)
{
    return strcmp(a, ((struct entry *)b)->hash);
}

int hashesC(const void *target, const void *elem)
{
    return strcmp((*(struct entry *)target).hash, (*(struct entry *)elem).hash);
}

int file_length(char *filename)
{

    struct stat info;
    int ret = stat(filename, &info);
    if(ret == -1)
    {
        return -1;
    }
    else
    {
        return info.st_size;
    }

}

struct entry *read_dictionary(char *filename, int *size)
{
    int len = file_length(filename);
    char *p = malloc(len);
    
    FILE *f = fopen(filename,"r");
    if(!f)
    {
        printf(" Can't open %s for reading.\n", filename);
        exit(1);
    }

    fread(p, sizeof(char), len, f);
    fclose(f);
    
    int num = 0;
    
    for(int i = 0; i < len; i++)
    {
        if(p[i] == '\n')
        {
            p[i] = '\0';
            num++;
        }
    }
    
    struct entry *ps = malloc(num * sizeof(struct entry));
     
    char *place = &p[0];
    char *holder = md5(place, strlen(place));
    strcpy(ps[0].password, place);
    strcpy(ps[0].hash, holder);
     
    int j = 1;
    for(int i = 0; i < len-1; i++)
    {
        if (p[i] == '\0')
        {
            char *something = &p[i+1];
            char *hashed = md5(something, strlen(something));
            strcpy(ps[j].password, something);
            strcpy(ps[j].hash, hashed);
            j++;
        }
         
    }
    
    *size = num;
    return ps;
}
int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    int dlen = 0;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    qsort(dict, dlen, sizeof(struct entry), hashesC);

    FILE *fp = fopen(argv[1],"r");
    if (!fp)
    {
        printf("Can't open %s for reading.\n", argv[1]);
        exit(1);
    }
    
    int count = 0;
    char line[100];
    while(fgets(line, 100, fp) != NULL)
    {
        if (line[strlen(line)-1] == '\n')
        {
            line[strlen(line)-1] = '\0';
        }
        
         struct entry *searcht = bsearch(line, dict, dlen, sizeof(struct entry), crackerF);
        
        if( searcht != NULL)
        {
            printf("%s %s\n", searcht->password, line);
            count++;
        }
    }
    printf("Passwords cracked: %d\n", count);
}